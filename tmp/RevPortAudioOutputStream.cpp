/*
 * RevPortAudioOutputStream.cpp
 *
 *  Created on: 12-11-2012
 *      Author: Revers
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>

#include <libresample/libresample.h>

#include <rev/common/RevErrorStream.h>
#include "RevPortAudioOutputStream.h"

using namespace std;
using namespace rev;

PortAudioOutputStream::PortAudioOutputStream() {
}

void PortAudioOutputStream::printDeviceInfo(const PaDeviceInfo* deviceInfo) {
    /* print device info fields */
    cout << "Name                        = " << deviceInfo->name << endl;
    cout << "Host API                    = "
            << Pa_GetHostApiInfo(deviceInfo->hostApi)->name << endl;
    cout << "Max inputs = " << deviceInfo->maxInputChannels << ", Max outputs = "
            << deviceInfo->maxOutputChannels << endl;

    cout << "Default low input latency   = " << deviceInfo->defaultLowInputLatency
            << endl;
    cout << "Default low output latency  = " << deviceInfo->defaultLowOutputLatency
            << endl;
    cout << "Default high input latency  = " << deviceInfo->defaultHighInputLatency
            << endl;
    cout << "Default high output latency = " << deviceInfo->defaultHighOutputLatency
            << endl;

    cout << "Default sample rate         = " << deviceInfo->defaultSampleRate
            << endl;
}

void PortAudioOutputStream::portAudioError(PaError err) {
    Pa_Terminate();
    cout << "An error occured while using the portaudio stream" << endl;
    cout << "Error number: " << err << endl;
    cout << "Error message: " << Pa_GetErrorText(err) << endl;
}

PaDeviceIndex PortAudioOutputStream::getOptimalDevice() {
    PaError err;
    const PaDeviceInfo* deviceInfo;
    int numDevices = Pa_GetDeviceCount();
    if (numDevices < 0) {
        err = numDevices;
        portAudioError(err);
        return paNoDevice ;
    }

    PaDeviceIndex device;
    bool deviceFound = false;
    cout << "Number of devices = " << numDevices << endl;
    for (int i = 0; i < numDevices && !deviceFound; i++) {
        deviceInfo = Pa_GetDeviceInfo(i);
        if (i == Pa_GetHostApiInfo(deviceInfo->hostApi)->defaultOutputDevice) {
            const PaHostApiInfo *hostInfo = Pa_GetHostApiInfo(deviceInfo->hostApi);
            if (string("Windows WASAPI") == hostInfo->name) {
                device = hostInfo->defaultOutputDevice;
                deviceFound = true;
            }
        }
    }

    if (!deviceFound) {
        device = Pa_GetDefaultOutputDevice(); /* default output device */
    }

    if (device == paNoDevice ) {
        cout << "ERROR: Device NOT FOUND!!" << endl;
        return paNoDevice ;
    }

    deviceInfo = Pa_GetDeviceInfo(device);
    printDeviceInfo(deviceInfo);

    return device;
}

#define DEL_ARR(x) if(x) { delete[] x; x = nullptr; }

void PortAudioOutputStream::initBuffers(int size) {
    srclen = size;
    ratio = dstrate / format.sampleRate;
    outBufferLength = (int) (ratio * (double) srclen + 1000.0);

    DEL_ARR(inBufferSingleCh);
    DEL_ARR(outBufferSingleCh);
    DEL_ARR(outBuffer);
    DEL_ARR(srcInBuffer);

    outBuffer = new float[outBufferLength * format.channels];
    srcInBuffer = new float[srclen * 2 * format.channels];
    srcInBufferRemainder = 0;
    inBufferSingleCh = new float[srclen];
    outBufferSingleCh = new float[outBufferLength];
}

/**
 * @Override
 */
bool PortAudioOutputStream::open(AudioFormat format) {
    assert(MAX_CHANNELS >= format.channels);

    if (inited) {
        close();
    }
    this->format = format;

    PaStreamParameters outputParameters;
    PaError err = 0;

    err = Pa_Initialize();
    if (err != paNoError) {
        portAudioError(err);
        return false;
    }

    printf("PortAudio version number = %d\nPortAudio version text = '%s'\n",
            Pa_GetVersion(), Pa_GetVersionText());

    PaDeviceIndex device = getOptimalDevice();
    if (!device == paNoDevice ) {
        return false;
    }
    const PaDeviceInfo* deviceInfo = Pa_GetDeviceInfo(device);

    dstrate = (double) deviceInfo->defaultSampleRate;

    initBuffers(DEFAULT_BUFFER_SIZE);

    printf("Source: %.2f Hz; Destination: %.2f Hz, ratio=%.5f\n",
            (double) format.sampleRate, dstrate, ratio);

    outputParameters.device = device;
    outputParameters.channelCount = format.channels; /* stereo output */
    outputParameters.sampleFormat = paFloat32;
    outputParameters.suggestedLatency =
            Pa_GetDeviceInfo(outputParameters.device)->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;
    int destSampleRate = deviceInfo->defaultSampleRate;
    err = Pa_OpenStream(
            &stream,
            NULL, /* no input */
            &outputParameters,
            destSampleRate, // SAMPLE_RATE,
            0,
            paClipOff, /* we won't output out of range samples so don't bother clipping them */
            NULL, /* no callback, use blocking API */
            NULL); /* no callback, so no callback userData */
    if (err != paNoError) {
        portAudioError(err);
        return false;
    }

    err = Pa_StartStream(stream);
    if (err != paNoError) {
        portAudioError(err);
        return false;
    }

    //----------------------------------------------------
//    SF_INFO dstinfo;
//    dstinfo.channels = format.channels;
//    dstinfo.samplerate = (int) dstrate;
//    dstinfo.format = SF_FORMAT_WAV | SF_FORMAT_FLOAT;
//    dstfile = sf_open("out.wav", SFM_WRITE, &dstinfo);
//    if (!dstfile) {
//        cout << "SNDFILE ERROR: " << sf_strerror(NULL) << endl;
//        return false;
//    }
    //=====================================================

    for (int c = 0; c < format.channels; c++) {
        handle[c] = (int*) resample_open(true, ratio, ratio);
    }

    return (inited = true);
}

/**
 * @Override
 */
void PortAudioOutputStream::close() {
    REV_TRACE_MSG("destroying audio context");
    if (!inited) {
        return;
    }

    for (int c = 0; c < format.channels; c++) {
        resample_close((void*) handle[c]);
    }

    PaError err = Pa_StopStream(stream);
    if (err != paNoError) {
        portAudioError(err);
    }

    err = Pa_CloseStream(stream);
    if (err != paNoError) {
        portAudioError(err);
    }

    Pa_Terminate();

    DEL_ARR(inBufferSingleCh);
    DEL_ARR(outBufferSingleCh);
    DEL_ARR(outBuffer);

//    if (dstfile) {
//        sf_close (dstfile);
//        dstfile = nullptr;
//    }

    inited = false;
}

/**
 * @Override
 */
bool PortAudioOutputStream::write(const unsigned char* buffer,
        int length) {
    assert(inited);

    float* srci;
    if (format.encoding != AudioEncoding::FLOAT_32) {
        // TODO: convert other formats to float
        alwaysAssert(format.encoding != AudioEncoding::FLOAT_32);
    } else {
        srci = (float*) buffer;
    }

    if (srclen < length) {
        initBuffers(length);
    }

    // pamietac ze to dziala tylko dla float
    memcpy(srcInBuffer + srcInBufferRemainder * format.channels,
            buffer,
            length * format.channels * sizeof(float));

    int inUsed = 0;
    int inUsed2 = 0;
    int actualOutBufferLen = 0;
    int actualOutBufferLen2 = 0;
    int inBlockSize = length + srcInBufferRemainder;

    for (int c = 0; c < format.channels; c++) {
        for (int i = 0; i < inBlockSize; i++) {
            inBufferSingleCh[i] = srcInBuffer[i * format.channels + c];
        }

        inUsed = 0;
        actualOutBufferLen = resample_process((void*) handle[c], ratio,
                inBufferSingleCh,
                inBlockSize, false,
                &inUsed, outBufferSingleCh, outBufferLength);

//        if (c == 0) {
//            inUsed2 = inUsed;
//            actualOutBufferLen2 = actualOutBufferLen;
//        } else {
//            if (inUsed2 != inUsed || actualOutBufferLen2 != actualOutBufferLen) {
//                fprintf(stderr, "Fatal error: format.channels out of sync!\n");
//                exit(-1);
//            }
//        }

        for (int i = 0; i < actualOutBufferLen; i++) {
            if (outBufferSingleCh[i] <= -1)
                outBuffer[i * format.channels + c] = -1;
            else if (outBufferSingleCh[i] >= 1)
                outBuffer[i * format.channels + c] = 1;
            else
                outBuffer[i * format.channels + c] = outBufferSingleCh[i];
        }
    }

    // sf_writef_float(dstfile, outBuffer, actualOutBufferLen);
    PaError err = Pa_WriteStream(stream, outBuffer, actualOutBufferLen);
    if (err != paNoError) {
        portAudioError(err);
        return -1;
    }

    srcInBufferRemainder = inBlockSize - inUsed;

    for (int i = 0; i < srcInBufferRemainder * format.channels; i++) {
        srcInBuffer[i] = srcInBuffer[i + (inUsed * format.channels)];
    }

    return (length != 0);
}

/**
 * @Override
 */
void PortAudioOutputStream::flush() {
    if (!inited) {
        return;
    }
}
