/*
 * RevPortAudioOutputStream.h
 *
 *  Created on: 12-11-2012
 *      Author: Revers
 */

#ifndef REVPORTAUDIOOUTPUTSTREAM_H_
#define REVPORTAUDIOOUTPUTSTREAM_H_

#include <iostream>
#include <rev/audio/RevAudioConfig.h>
#include <rev/audio/RevAudioFormat.hpp>
#include <rev/audio/RevIAudioOutputStream.hpp>

#include <rev/common/RevAssert.h>
#include <portaudio/portaudio.h>

//#include <sndfile/sndfile.h>

namespace rev {

    class REVAUDIO_API PortAudioOutputStream: public IAudioOutputStream {
    private:
        static const int DEFAULT_BUFFER_SIZE = 4096;
        static const int MAX_CHANNELS = 8;

        PaStream* stream = NULL;
        double dstrate = 0.0;
        double ratio = 0.0;
        int* handle[MAX_CHANNELS];
        int srclen = 0;
        int outBufferLength = DEFAULT_BUFFER_SIZE;
        float* inBufferSingleCh = nullptr;
        float* outBufferSingleCh = nullptr;
        float* outBuffer = nullptr;
        float* srcInBuffer = nullptr;
        int srcInBufferRemainder = 0;

        bool inited = false;
        AudioFormat format;

        // ------------------------------
        // SNDFILE* dstfile = nullptr;
        // ==============================

        PortAudioOutputStream(const PortAudioOutputStream& orig);

    public:

        PortAudioOutputStream();

        ~PortAudioOutputStream() {
            close();
        }

        /**
         * @Override
         */
        virtual bool open(AudioFormat format);

        /**
         * @Override
         */
        virtual bool reopen(AudioFormat format) {
            close();
            return open(format);
        }

        /**
         * @Override
         */
        virtual void close();

        /**
         * @Override
         */
        virtual bool write(const unsigned char* buffer, int length);

        /**
         * @Override
         */
        virtual void flush();

        /**
         * @Override
         */
        virtual AudioFormat getFormat() {
            assert(inited);
            return format;
        }

    private:
        void printDeviceInfo(const PaDeviceInfo* deviceInfo);
        void portAudioError(PaError err);
        PaDeviceIndex getOptimalDevice();

        void initBuffers(int size);

    };
} // namespace rev

#endif /* REVPORTAUDIOOUTPUTSTREAM_H_ */
