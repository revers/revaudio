/*
 * RevIAudioSeekableInputStream.hpp
 *
 *  Created on: 03-10-2012
 *      Author: Revers
 */

#ifndef REVIAUDIOSEEKABLEINPUTSTREAM_H_
#define REVIAUDIOSEEKABLEINPUTSTREAM_H_

#include "RevAudioFormat.h"
#include "RevIAudioInputStream.h"

namespace rev {

class IAudioSeekableInputStream: public IAudioInputStream {
public:

	/**
	 * Current position in samples.
	 */
	virtual offset_t getPosition() = 0;

	/**
	 * Seeks to the "samplePosition" sample.
	 */
	virtual bool seek(offset_t samplePosition) = 0;

	/**
	 * Skips next "samples" number of samples.
	 * Equivalent of seek(getPosition() + samples);
	 */
	virtual bool skip(offset_t samples) = 0;

	/**
	 * Reset position to the beginning.
	 * Equivalent of seek(0);
	 */
	virtual bool reset() = 0;
};

} /* namespace rev */

#endif /* REVIAUDIOSEEKABLEINPUTSTREAM_H_ */
