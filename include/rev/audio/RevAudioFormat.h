/*
 * RevAudioFormat.hpp
 *
 *  Created on: 03-10-2012
 *      Author: Revers
 */

#ifndef REVAUDIOFORMAT_H_
#define REVAUDIOFORMAT_H_

#include <iostream>

namespace rev {

enum class AudioEncoding {
	SIGNED_8, UNSIGNED_8, SIGNED_16, UNSIGNED_16, FLOAT_32
};

struct AudioFormat {
	int sampleRate = 0;
	int channels = 0;
	AudioEncoding encoding = AudioEncoding::UNSIGNED_16;

	AudioFormat() {
	}

	AudioFormat(int sampleRate, int channels, AudioEncoding encoding) :
			sampleRate(sampleRate), channels(channels), encoding(encoding) {
	}

	void set(int sampleRate, int channels, AudioEncoding encoding) {
		this->sampleRate = sampleRate;
		this->channels = channels;
		this->encoding = encoding;
	}

	int getBytesPerSample() {
		switch (encoding) {
		case AudioEncoding::SIGNED_8:
			return 1;
		case AudioEncoding::UNSIGNED_8:
			return 1;
		case AudioEncoding::SIGNED_16:
			return 2;
		case AudioEncoding::UNSIGNED_16:
			return 2;
		case AudioEncoding::FLOAT_32:
			return 4;
		default:
			return 0;
		}
	}

	/**
	 * @return getBytesPerSample() * channels.
	 */
	int getFrameSize() {
		return getBytesPerSample() * channels;
	}
};
} /* namespace rev */

inline std::ostream& operator<<(std::ostream& out,
		const rev::AudioEncoding& encoding) {
	switch (encoding) {
	case rev::AudioEncoding::SIGNED_8: {
		out << "AudioEncoding::SIGNED_8";
		break;
	}
	case rev::AudioEncoding::UNSIGNED_8: {
		out << "AudioEncoding::UNSIGNED_8";
		break;
	}
	case rev::AudioEncoding::SIGNED_16: {
		out << "AudioEncoding::SIGNED_16";
		break;
	}
	case rev::AudioEncoding::UNSIGNED_16: {
		out << "udioEncoding::UNSIGNED_16";
		break;
	}
	case rev::AudioEncoding::FLOAT_32: {
		out << "AudioEncoding::FLOAT_32";
		break;
	}
	default: {
		out << "RevAudioFormat.hpp.operator<<( ??? )";
		break;
	}
	}

	return out;
}

inline std::ostream& operator<<(std::ostream& out,
		const rev::AudioFormat& format) {
	out << "AudioFormat [sampleRate=" << format.sampleRate << ", channels="
			<< format.channels << ", encoding=" << format.encoding << "]";
	return out;
}

#endif /* REVAUDIOFORMAT_H_ */
