/* 
 * File:   RevIAudioOutputStream.hpp
 * Author: Revers
 *
 * Created on 2 październik 2012, 21:35
 */

#ifndef REVIAUDIOOUTPUTSTREAM_H
#define	REVIAUDIOOUTPUTSTREAM_H

#include "RevAudioFormat.h"

namespace rev {

    /**
     * WAV audio output stream.
     */
    class IAudioOutputStream {
    public:
        virtual ~IAudioOutputStream() {
        }

        virtual bool open(AudioFormat format) = 0;

        virtual bool reopen(AudioFormat format) = 0;

        virtual bool write(const unsigned char* buffer, int length) = 0;

        virtual void flush() = 0;

        virtual void close() = 0;

        virtual AudioFormat getFormat() = 0;
    };

} /* namespace rev */

#endif	/* REVIAUDIOOUTPUTSTREAM_H */

