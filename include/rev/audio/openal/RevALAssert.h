/* 
 * File:   RevALAssert.h
 * Author: Revers
 *
 * Created on 27 lipiec 2012, 21:34
 */

#ifndef REVALASSERT_H
#define	REVALASSERT_H

#include <AL/al.h>

#include <rev/audio/RevAudioConfig.h>

namespace rev {
    REVAUDIO_API const char* alGetErrorString(ALenum code);
	REVAUDIO_API void alAssert_(const char* function, const char* file, int line);
    REVAUDIO_API void alAssertNoExit_(const char* function, const char* file, int line);
}

#define REVAUDIO_DEBUG_FILE __FILE__
#define REVAUDIO_DEBUG_LINE __LINE__
#define REVAUDIO_DEBUG_FUNCTION __PRETTY_FUNCTION__

#if !defined(NDEBUG) && !defined(REVAUDIO_NO_ASSERT) && !defined(REVAUDIO_ASSERT_NO_EXIT) 

/**
 * if AL error occurs (alGetError() != AL_NO_ERROR), error message
 * is printed out and application exits with -1 error code.
 */
#define alAssert rev::alAssert_(REVAUDIO_DEBUG_FUNCTION, REVAUDIO_DEBUG_FILE, REVAUDIO_DEBUG_LINE)

/**
 * if AL error occurs (alGetError() != AL_NO_ERROR), 
 * error message is printed out.
 */
#define alAssertNoExit rev::alAssertNoExit_(REVAUDIO_DEBUG_FUNCTION, REVAUDIO_DEBUG_FILE, REVAUDIO_DEBUG_LINE)


#elif !defined(NDEBUG) && !defined(REVAUDIO_NO_ASSERT)
/**
 * if AL error occurs (alGetError() != AL_NO_ERROR), 
 * error message is printed out.
 */
#define alAssert rev::alAssertNoExit_(REVAUDIO_DEBUG_FUNCTION, REVAUDIO_DEBUG_FILE, REVAUDIO_DEBUG_LINE)

/**
 * if AL error occurs (alGetError() != AL_NO_ERROR), 
 * error message is printed out.
 */
#define alAssertNoExit rev::alAssertNoExit_(REVAUDIO_DEBUG_FUNCTION, REVAUDIO_DEBUG_FILE, REVAUDIO_DEBUG_LINE)

#else 
/**
 * Empty macro definitions.
 */
#define alAssert ((void)0)
#define alAssertNoExit ((void)0)
#endif

/**
 * Works even if there is NDEBUG flag set. 
 * If AL error occurs (alGetError() != AL_NO_ERROR), error message
 * is printed out and application exits with -1 error code.
 */
#define alAlwaysAssert rev::alAssert_(REVAUDIO_DEBUG_FUNCTION, REVAUDIO_DEBUG_FILE, REVAUDIO_DEBUG_LINE)

/**
 * Works even if there is NDEBUG flag set. 
 * If AL error occurs (alGetError() != AL_NO_ERROR), error message
 * is printed out.
 */
#define alAlwaysAssertNoExit rev::alAssertNoExit_(REVAUDIO_DEBUG_FUNCTION, REVAUDIO_DEBUG_FILE, REVAUDIO_DEBUG_LINE)

#endif	/* REVALASSERT_H */

