/*
 * RevIAudioInputStream.hpp
 *
 *  Created on: 03-10-2012
 *      Author: Revers
 */

#ifndef REVIAUDIOINPUTSTREAM_H_
#define REVIAUDIOINPUTSTREAM_H_

#include <string>

#include "RevAudioConfig.h"
#include "RevAudioFormat.h"

namespace rev {

class IAudioInputStream {
public:

	virtual ~IAudioInputStream() {
	}

	virtual bool open(const char* filename) = 0;

	/**
	 *
	 * Tries to read "length" bytes from the stream, though
	 * this amount is not guaranteed (hence variable "bytesRead").
	 *
	 * if *bytesRead == 0 and read() returns true, then there
	 * is no more data (stream reached the end).
	 */
	virtual bool read(unsigned char* buffer, int length,
			int* bytesRead) = 0;

	virtual void close() = 0;

	virtual AudioFormat getFormat() = 0;

	virtual const std::string& getErrorMessage() = 0;

	/**
	 * Return, if possible, the full (expected) length of current track
	 * in samples. Otherwise, it returns 0.
	 */
	virtual offset_t getLength() = 0;

};
} /* namespace rev */

#endif /* REVIAUDIOINPUTSTREAM_H_ */
