#ifndef REVAUDIO_CONFIG_H
#define	REVAUDIO_CONFIG_H

#define REVAUDIO_VERSION_MAJOR 0
#define REVAUDIO_VERSION_MINOR 1 

#ifndef REVAUDIO_STATIC
/* #undef REVAUDIO_STATIC */
#endif

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(_WIN64)
#define REVAUDIO_WINDOWS
#endif

#if defined(REVAUDIO_WINDOWS)
#   define REVAUDIO_CDECL_CALL    __cdecl
#   define REVAUDIO_STD_CALL      __stdcall
#   define REVAUDIO_CALL          REVAUDIO_CDECL_CALL
#   define REVAUDIO_EXPORT_API    __declspec(dllexport)
#   define REVAUDIO_IMPORT_API    __declspec(dllimport)
#else
#   define REVAUDIO_CDECL_CALL
#   define REVAUDIO_STD_CALL
#   define REVAUDIO_CALL
#   define REVAUDIO_EXPORT_API
#   define REVAUDIO_IMPORT_API
#endif

#if defined REVAUDIO_EXPORTS
#   define REVAUDIO_API REVAUDIO_EXPORT_API
#elif defined REVAUDIO_STATIC
#   define REVAUDIO_API
#   if defined(_MSC_VER) && !defined(REVAUDIO_NO_LIB_PRAGMA)
#       ifdef _WIN64
#           pragma comment(lib, "RevAudioStatic64")
#       else
#           pragma comment(lib, "RevAudioStatic")
#       endif
#   endif
#else
#   define REVAUDIO_API REVAUDIO_IMPORT_API
#   if defined(_MSC_VER) && !defined(REVAUDIO_NO_LIB_PRAGMA)
#       ifdef _WIN64
#           pragma comment(lib, "RevAudio64")
#       else
#           pragma comment(lib, "RevAudio")
#       endif
#   endif
#endif

#ifdef NDEBUG
#include <rev/common/RevErrorStream.h>
#define REVAUDIO_TRACE(x) ((void)0)
#define REVAUDIO_ERR_MSG(x) REV_ERROR_MSG(x)

#else

#include <iostream>
#define REVAUDIO_TRACE(x) std::cout << x << std::endl
#include <rev/common/RevAssert.h>
#define REVAUDIO_ERR_MSG(x) revAssertMsg(false, x)
#endif


#include <cstdint>
#ifdef REVAUDIO_OFFSET_32
typedef int32_t offset_t;
#else
typedef int64_t offset_t;
#endif

#endif /* REVAUDIO_CONFIG_H */
