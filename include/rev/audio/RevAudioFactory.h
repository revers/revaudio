/*
 * RevAudioFactory.h
 *
 *  Created on: 03-10-2012
 *      Author: Revers
 */

#ifndef REVAUDIOFACTORY_H_
#define REVAUDIOFACTORY_H_

#include <memory>
#include <rev/audio/RevAudioConfig.h>
#include <rev/audio/RevIAudioSeekableInputStream.h>
#include <rev/audio/RevIAudioOutputStream.h>

namespace rev {

typedef std::shared_ptr<IAudioSeekableInputStream> IAudioSeekableInputStreamPtr;
typedef std::shared_ptr<IAudioOutputStream> IAudioOutputStreamPtr;

class REVAUDIO_API AudioFactory {
private:
	AudioFactory() {
	}
	virtual ~AudioFactory() {
	}
public:
	static IAudioSeekableInputStreamPtr createMP3FileInputStream(
			const char* filename);

	static IAudioOutputStreamPtr getDefaultAudioOutputStream();
};
} /* namespace rev */
#endif /* REVAUDIOFACTORY_H_ */
