/*
 * RevAudioFactory.cpp
 *
 *  Created on: 03-10-2012
 *      Author: Revers
 */

#include <rev/audio/RevAudioFactory.h>

#include "RevMP3AudioInputStream.h"
#include "RevOpenALOutputStream.h"

using namespace rev;

IAudioSeekableInputStreamPtr AudioFactory::createMP3FileInputStream(
		const char* filename) {

	MP3AudioInputStream* mp3Stream = new MP3AudioInputStream();
	IAudioSeekableInputStreamPtr ptr(mp3Stream);

	if (!mp3Stream->open(filename)) {
		return nullptr;
	}

	return ptr;
}

IAudioOutputStreamPtr AudioFactory::getDefaultAudioOutputStream() {
	//return IAudioOutputStreamPtr(new rev::PortAudioOutputStream());
	return IAudioOutputStreamPtr(new rev::OpenALOutputStream());
}
