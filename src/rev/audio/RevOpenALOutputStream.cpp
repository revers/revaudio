/*
 * RevOpenALOutputStream.cpp
 *
 *  Created on: 04-10-2012
 *      Author: Revers
 */
#include <cstring>

#include <AL/al.h>
#include <AL/alc.h>

#include "RevOpenALOutputStream.h"

#include <rev/common/RevSleep.h>
#include <rev/audio/openal/RevALAssert.h>

using namespace rev;

#define NUM_BUFFERS 2

#ifndef AL_FORMAT_MONO_FLOAT32
#define AL_FORMAT_MONO_FLOAT32 0x10010
#endif
#ifndef AL_FORMAT_STEREO_FLOAT32
#define AL_FORMAT_STEREO_FLOAT32 0x10011
#endif

typedef struct {
    ALCdevice* device;
    ALCcontext* context;
    ALuint source;
    ALuint buffer;
    ALenum format;
    ALsizei sampleRate;
} openal_data_t;

/**
 * @Override
 */
bool OpenALOutputStream::open(AudioFormat format) {
    if (inited) {
        REVAUDIO_TRACE("WARNING: You are trying to init "
        "initialized already OpenALOutputStream");
        return true;
    }
    this->format = format;

    audioStruct = malloc(sizeof(openal_data_t));
    memset(audioStruct, 0, sizeof(openal_data_t));

    openal_data_t& openAL = *((openal_data_t*) audioStruct);

    /* Open and initialize a device with default settings */
    openAL.device = alcOpenDevice(NULL);
    //alAssert;

    if (!openAL.device) {
        REVAUDIO_ERR_MSG("OpenAL ERROR: Could not open a device!\n");
        return false;
    }

    openAL.context = alcCreateContext(openAL.device, NULL);
  //  alAssert;
    if (!openAL.context) {
        REVAUDIO_ERR_MSG("OpenAL ERROR: Could not create a context!\n");
        return false;
    }

    ALboolean succ = alcMakeContextCurrent(openAL.context);
    alAssert;

    if (succ == ALC_FALSE) {
        alcDestroyContext(openAL.context);
        alcCloseDevice(openAL.device);

        REVAUDIO_ERR_MSG("OpenAL ERROR: Could not set a context.!\n");
        return false;
    }

    alGenSources(1, &openAL.source);
    alAssert;

    openAL.sampleRate = format.sampleRate;

    if (format.encoding == AudioEncoding::SIGNED_16
            && format.channels == 2) {
        openAL.format = AL_FORMAT_STEREO16;
    } else if (format.encoding == AudioEncoding::SIGNED_16
            && format.channels == 1) {
        openAL.format = AL_FORMAT_MONO16;
    } else if (format.encoding == AudioEncoding::UNSIGNED_8
            && format.channels == 2) {
        openAL.format = AL_FORMAT_STEREO8;
    } else if (format.encoding == AudioEncoding::UNSIGNED_8
            && format.channels == 1) {
        openAL.format = AL_FORMAT_MONO8;
    } else if (format.encoding == AudioEncoding::FLOAT_32
            && format.channels == 2) {
        openAL.format = AL_FORMAT_STEREO_FLOAT32;
    } else if (format.encoding == AudioEncoding::FLOAT_32
            && format.channels == 1) {
        openAL.format = AL_FORMAT_MONO_FLOAT32;
    } else {
        alwaysAssertMsg(false, "UNKNOWN AUDIO FORMAT!! "
                << rev::a2s(R((int) format.encoding), R(format.channels)));
    }

    return (inited = true);
}

/**
 * @Override
 */
void OpenALOutputStream::close() {
    REVAUDIO_TRACE("destroying audio context");
    if (!inited) {
        return;
    }

    ALCdevice *device;
    ALCcontext *ctx;

    /* Close the device belonging to the current context, and destroy the
     * context. */
    ctx = alcGetCurrentContext();

    if (ctx == NULL)
        return;

    device = alcGetContextsDevice(ctx);
    alcMakeContextCurrent(NULL);
    alcDestroyContext(ctx);
    alcCloseDevice(device);

    inited = false;

    if (audioStruct) {
        free(audioStruct);
        audioStruct = nullptr;
    }
}

/**
 * @Override
 */
bool OpenALOutputStream::write(const unsigned char* buffer,
        int length) {
    assert(inited);

    ALint state, n;
    openal_data_t& openAL = *((openal_data_t*) audioStruct);

    alGetSourcei(openAL.source, AL_BUFFERS_QUEUED, &n);
    alAssert;
    if (n < NUM_BUFFERS) {
        alGenBuffers(1, &openAL.buffer);
        alAssert;
    } else {
        alGetSourcei(openAL.source, AL_SOURCE_STATE, &state);
        alAssert;
        if (state != AL_PLAYING) {
            alSourcePlay(openAL.source);
            alAssert;
        }
        while (alGetSourcei(openAL.source, AL_BUFFERS_PROCESSED, &n), n == 0) {
            //usleep(10 000);
            rev::sleep(10);
        }
        alSourceUnqueueBuffers(openAL.source, 1, &openAL.buffer);
        alAssert;
    }

    alBufferData(openAL.buffer, openAL.format, buffer, length, openAL.sampleRate);
    alAssert;
    int written;
    alSourceQueueBuffers(openAL.source, 1, &openAL.buffer);
    alAssert;

    return (length != 0);
}

/**
 * @Override
 */
void OpenALOutputStream::flush() {
    if (!inited) {
        return;
    }

    openal_data_t& openAL = *((openal_data_t*) audioStruct);

    /* stop playing and flush all buffers */
    alSourceStop(openAL.source);
    alAssert;

    ALint n;
    while (alGetSourcei(openAL.source, AL_BUFFERS_PROCESSED, &n), n > 0) {
        alSourceUnqueueBuffers(openAL.source, 1, &openAL.buffer);
        alAssert;
        alDeleteBuffers(1, &openAL.buffer);
        alAssert;
    }
}

