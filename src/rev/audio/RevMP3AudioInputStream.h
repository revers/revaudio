/*
 * RevMP3AudioInputStream.h
 *
 *  Created on: 03-10-2012
 *      Author: Revers
 */

#ifndef REVMP3AUDIOINPUTSTREAM_H_
#define REVMP3AUDIOINPUTSTREAM_H_

#include <rev/audio/RevAudioConfig.h>
#include <rev/audio/RevAudioFormat.h>
#include <rev/audio/RevIAudioSeekableInputStream.h>

#include <mpg123/mpg123.h>

namespace rev {

class REVAUDIO_API MP3AudioInputStream: public IAudioSeekableInputStream {
	AudioFormat format;
	offset_t position = 0;

	mpg123_handle* mpgHandle = nullptr;
	int mpg123Encoding = 0;
	off_t err = MPG123_OK;
	offset_t length = 0;
	offset_t sampleSize = 0;
	std::string errMessage;

public:
	MP3AudioInputStream() {
	}
	virtual ~MP3AudioInputStream() {
		cleanup();
	}

	/**
	 * bufferSize = 0 means default buffer size.
	 */
	bool open(const char* filename) override;

	/**
	 * @Override
	 *
	 * Tries to read "length" bytes from the stream, though
	 * this amount is not always guaranteed (hence variable "bytesRead").
	 *
	 * if *bytesRead == 0 and read() returns true, then there
	 * is no more data (stream reached the end).
	 *
	 */
	bool read(unsigned char* buffer, int length,
			int* bytesRead) override;

	void close() override {
		cleanup();
	}

	AudioFormat getFormat() override {
		return format;
	}

	/**
	 * Current position in samples.
	 */
	offset_t getPosition() override {
		return (offset_t) mpg123_tell(mpgHandle);
	}

	/**
	 * Seeks to the "samplePosition" sample.
	 */
	bool seek(offset_t samplePosition) override;

	/**
	 * Skips next "samples" number of samples.
	 * Equivalent of seek(getPosition() + samples);
	 */
	bool skip(offset_t samples) override {
		return seek(getPosition() + samples);
	}

	/**
	 * Reset position to the beginning.
	 * Equivalent of seek(0);
	 */
	bool reset() override {
		return seek(offset_t(0));
	}

	/**
	 * Return, if possible, the full (expected) length of current track
	 * in samples. Otherwise, it returns 0.
	 */
	offset_t getLength() override {
		return length;
	}

	const std::string& getErrorMessage() override;

private:
	void cleanup();
};

} /* namespace rev */
#endif /* REVMP3AUDIOINPUTSTREAM_H_ */
