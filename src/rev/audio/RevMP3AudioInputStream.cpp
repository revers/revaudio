/*
 * RevMP3AudioInputStream.cpp
 *
 *  Created on: 03-10-2012
 *      Author: Revers
 */

#include "RevMP3AudioInputStream.h"
#include <fstream>

using namespace std;
using namespace rev;

static const bool useFloat = true;

bool MP3AudioInputStream::open(const char* filename) {
	ifstream in(filename);
	if (!in) {
		REVAUDIO_ERR_MSG("ERROR File Not Found: MP3 FILE '"
				<< filename << "' DOES NOT EXIST!");
		return false;
	} else {
		// file exists.
		in.close();
	}

	int err = MPG123_OK;

	err = mpg123_init();
	if (err != MPG123_OK || (mpgHandle = mpg123_new(NULL, &err)) == NULL) {
		REVAUDIO_ERR_MSG(
				"MPG123 ERROR: Basic setup goes wrong: " << mpg123_plain_strerror(err));
		cleanup();
		return false;
	}

	if (useFloat) {
		/* Simple hack to enable floating point output. */
		mpg123_param(mpgHandle, MPG123_ADD_FLAGS, MPG123_FORCE_FLOAT, 0.);
		mpg123Encoding = MPG123_ENC_FLOAT_32;
		format.encoding = AudioEncoding::FLOAT_32;
	} else {
		mpg123Encoding = MPG123_ENC_SIGNED_16;
		format.encoding = AudioEncoding::SIGNED_16;
	}

	/* Let mpg123 work with the file, that excludes MPG123_NEED_MORE messages. */
	err = mpg123_open(mpgHandle, filename);
	if (err != MPG123_OK) {
		REVAUDIO_ERR_MSG(
				"MPG123 ERROR: mpg123_open() FAILED: " << mpg123_plain_strerror(err));
		cleanup();
		return false;
	}

	/* Peek into track and get first output format. */
	err = mpg123_getformat(mpgHandle, (long*) &format.sampleRate,
			(int*) &format.channels, &mpg123Encoding);

	if (err != MPG123_OK) {
		REVAUDIO_ERR_MSG(
				"MPG123 ERROR: mpg123_getformat() FAILED: " << mpg123_strerror(mpgHandle));
		cleanup();
		return false;
	}

	length = (offset_t) mpg123_length(mpgHandle);
	sampleSize = format.getFrameSize();

	/* Ensure that this output format will not change (it could,
	 * when we allow it). */
	mpg123_format_none(mpgHandle);
	mpg123_format(mpgHandle, format.sampleRate, format.channels, mpg123Encoding);

	/* Buffer could be almost any size here, mpg123_outblock() is just some recommendation.
	 Important, especially for sndfile writing, is that the size is a multiple of sample size. */
//    if (bufferSize == 0) {
//        this->bufferSize = bufferSize = mpg123_outblock(mpgHandle);
//    }
//
//    buffer = (unsigned char*) malloc(bufferSize);
	REVAUDIO_TRACE(
			"Creating "
			<< (useFloat ? "FLOAT_32" : "SIGNED_16")
			<< " WAV with "
			<< rev::a2s(R(format.channels), R(format.sampleRate), R(filename), R(length)));

	return true;
}

//off_t samples = 0; = position
void MP3AudioInputStream::cleanup() {
	if (mpgHandle != nullptr) {
		mpg123_close(mpgHandle);
		mpg123_delete(mpgHandle);
		mpg123_exit();
		mpgHandle = nullptr;
	}
}

/**
 * @Override
 *
 * Tries to read "length" bytes from the stream, though
 * this amount is not always guaranteed (hence variable "bytesRead").
 *
 * if *bytesRead == 0 and MP3AudioInputStream::read() returns true, then there
 * is no more data (stream reached the end).
 *
 */
bool MP3AudioInputStream::read(unsigned char* buffer, int length,
		int* bytesRead) {

	err = mpg123_read(mpgHandle, buffer, (size_t) length, (size_t*) bytesRead);
	if (err == MPG123_DONE) {
		*bytesRead = 0;
		return true;
	}

	return (err == MPG123_OK);
}

/**
 * @Override
 */
const std::string& MP3AudioInputStream::getErrorMessage() {
	errMessage = mpg123_plain_strerror(err);
	return errMessage;
}

/**
 * Seeks to the "samplePosition" sample.
 */
bool MP3AudioInputStream::seek(offset_t samplePosition) {
	err = mpg123_seek(mpgHandle, (off_t) samplePosition, SEEK_SET);
	return err >= 0;
}

