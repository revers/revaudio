
#include <cstdio>
#include <cstdlib>

#include <rev/common/RevAssert.h>
#include <rev/common/RevCommonUtil.h>
#include <rev/common/RevErrorStream.h>

#include <rev/audio/openal/RevALAssert.h>

using namespace rev;

const char* rev::alGetErrorString(ALenum val) {
    switch (val) {
        case AL_NO_ERROR:
            return "AL_NO_ERROR";
        case AL_INVALID_NAME:
            return "AL_INVALID_NAME";
        case AL_INVALID_ENUM:
            return "AL_INVALID_ENUM";
        case AL_INVALID_VALUE:
            return "AL_INVALID_VALUE";
        case AL_INVALID_OPERATION:
            return "AL_INVALID_OPERATION";
        case AL_OUT_OF_MEMORY:
            return "AL_OUT_OF_MEMORY";
        default:
            return "REV_UNKNOWN_ERROR";
    }
}

void rev::alAssert_(const char* function, const char * file, int line) {

    ALenum alErr = alGetError();
    if (alErr != AL_NO_ERROR) {
        REV_ERROR_MSG("OPENAL ERROR " << rev::alGetErrorString(alErr)
                << "; " << function << "; "
                << "\n\t" << file << " @ line " << line << "\n");
        fflush(stdout);

        __asm("int $3");
        exit(-1);
    }
}

void rev::alAssertNoExit_(const char* function, const char * file, int line) {

    ALenum alErr = alGetError();
    while (alErr != AL_NO_ERROR) {

        REV_ERROR_MSG("OPENAL ERROR " << rev::alGetErrorString(alErr)
                << "; " << function << "; "
                << "\n\t" << file << " @ line " << line << "\n");
        fflush(stdout);
        alErr = alGetError();
    }
}

