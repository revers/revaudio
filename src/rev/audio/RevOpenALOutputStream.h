/*
 * RevOpenALOutputStream.h
 *
 *  Created on: 04-10-2012
 *      Author: Revers
 */

#ifndef REVOPENALOUTPUTSTREAM_H_
#define REVOPENALOUTPUTSTREAM_H_

#include <iostream>
#include <rev/audio/RevAudioConfig.h>
#include <rev/audio/RevAudioFormat.h>
#include <rev/audio/RevIAudioOutputStream.h>

#include <rev/common/RevAssert.h>

namespace rev {

    class REVAUDIO_API OpenALOutputStream: public IAudioOutputStream {
    private:
        bool inited = false;
        AudioFormat format;

        void* audioStruct = nullptr;

        OpenALOutputStream(const OpenALOutputStream& orig);

    public:

        OpenALOutputStream() {
        }

        ~OpenALOutputStream() {
            close();
        }

        /**
         * @Override
         */
        virtual bool open(AudioFormat format);

        /**
         * @Override
         */
        virtual bool reopen(AudioFormat format) {
            close();
            return open(format);
        }

        /**
         * @Override
         */
        virtual void close();

        /**
         * @Override
         */
        virtual bool write(const unsigned char* buffer, int length);

        /**
         * @Override
         */
        virtual void flush();

        /**
         * @Override
         */
        virtual AudioFormat getFormat() {
            assert(inited);
            return format;
        }
    private:

    };
} // namespace rev
#endif /* REVOPENALOUTPUTSTREAM_H_ */
